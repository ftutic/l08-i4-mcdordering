﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders
{
    internal class Narudzbe
    {
        public List<Narudzba> NarudzbaList { get; set; }

        public List<Narudzba> GotovaNarudzbaList { get; set; }

        private int uniqueBrNarudzbe;
        public Narudzbe()
        {
            NarudzbaList = new List<Narudzba>();
            GotovaNarudzbaList = new List<Narudzba>();
            uniqueBrNarudzbe = 1;
        }

        public void newNarudzba(String tip, int vrijemeIzrade) {
            Narudzba n = new Narudzba(uniqueBrNarudzbe,tip,vrijemeIzrade);
            uniqueBrNarudzbe++;
            NarudzbaList.Add(n);
            novaNarudzbaNaListi?.Invoke(this, new EventArgs());
            Task izrada = new Task(() => {
                    Thread.Sleep(2000+(n.VrijemeIzrade*1000));
                    NarudzbaList.Remove(n);
                    GotovaNarudzbaList.Add(n);
                    gotovaNarudzba?.Invoke(this, new EventArgs());

            });
            izrada.Start();

    }

        public delegate void NarudzbaDelegate(Object sender, EventArgs e);
        public event NarudzbaDelegate novaNarudzbaNaListi;
        public event NarudzbaDelegate gotovaNarudzba;

    }
}
