﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders
{
    internal class Narudzba
    {
        public int BrNarudzbe { get; set; }
        public string Vrsta { get; set; }
        public int VrijemeIzrade { get; set; }


        public Narudzba(int brNarudzbe,string vrsta, int vrijemeIzrade) { 
            BrNarudzbe = brNarudzbe;
            Vrsta = vrsta;
            VrijemeIzrade = vrijemeIzrade;
        }



        public override String ToString() {
            return BrNarudzbe.ToString("D3")+ " ("+Vrsta+")";
        }
    }
}
