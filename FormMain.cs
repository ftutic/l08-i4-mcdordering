namespace McOrders
{
    public partial class FormMain : Form
    {
        Narudzbe mcNarudzbe;
        public FormMain()
        {
            InitializeComponent();
            mcNarudzbe = new Narudzbe();
            mcNarudzbe.novaNarudzbaNaListi += Narudzba_updateOrdersList;
            mcNarudzbe.gotovaNarudzba += Narudzba_updateOrdersList;


        }
        private void Narudzba_updateOrdersList(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { updateOrdersList(); }));
            }
            else { updateOrdersList(); }
        }


        private void updateOrdersList()
        {
            lbActiveOrders.Items.Clear();
            lbCompletedOrders.Items.Clear();

            /*
             * 
             * Ne radi pri button spamu
            mcNarudzbe.NarudzbaList.ForEach(delegate (Narudzba n) { lbActiveOrders.Items.Add(n.ToString()); });
            mcNarudzbe.GotovaNarudzbaList.ForEach(delegate (Narudzba n) { lbCompletedOrders.Items.Add(n.ToString()); });
            */
            List<Narudzba> nList = mcNarudzbe.NarudzbaList;
            List<Narudzba> gList = mcNarudzbe.GotovaNarudzbaList;
            nList.ForEach(delegate (Narudzba n) { lbActiveOrders.Items.Add(n.ToString()); });
            gList.ForEach(delegate (Narudzba n) { lbCompletedOrders.Items.Add(n.ToString()); });

        }

        private void btnPommes_Click(object sender, EventArgs e)
        {
            mcNarudzbe.newNarudzba("Pommes", 5);
        }

        private void btnBigMac_Click(object sender, EventArgs e)
        {

            mcNarudzbe.newNarudzba("Big Mac", 10);
        }

        private void btnDrink_Click(object sender, EventArgs e)
        {

            mcNarudzbe.newNarudzba("Drink", 2);
        }
    }
}